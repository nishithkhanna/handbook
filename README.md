# /e/Foundation Handbook

Welcome to the GitLab Handbook.  This repository contains all of the content for the Handbook on https://doc.e.foundation 

## Using th handbook

### Run the handbook locally for edits

#### Supported methods:
- Docker or compatible runtime (all software preinstalled in a container image)
- Local installation (experienced users, more software dependencies, can break)

#### Docker Based

1. Requirements

- Command line: git, wget (for syncing data). Additional requirements for local installation, see below.
- Docker or compatible runtime (for running the Hugo environment in a container)
  - On macOS: Docker Desktop, Rancher Desktop, etc. 
  - On Linux: Docker engine, Podman, etc.

2. Clone the handbook Git repository

Cloning the repository allows you to manually edit the handbook locally. If you prefer to use the Web IDE, 
please continue reading the editing the handbook documentation. We recommend using git to clone the repository and
then editing the handbook with a text editor such as Visual Studio Code, Typora, Nova or Sublime to name a few.

3. Clone the repository:
```
git clone git@gitlab.com:gitlab-com/content-sites/handbook.git
```

4. Sync the theme
```shell
git submodule init
git submodule update
```

5. Start the server
```shell
docker compose up -d
```

#### Local Installation

TBD

