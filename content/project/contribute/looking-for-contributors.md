---
weight: 1
---

# Projects looking for contributors

All Murena projects welcome contributors! Look for your preferred project and register for user [on this website](https://gitlab.e.foundation) to contribute!

## Open Positions at Murena

Positions are full time, and remote working.

Look for the list of open positions and how to apply [on the Jobs page](https://murena.com/jobs/).

## Specific projects looking for contributors

We have several projects that need contributors:

- Help the [microG project](https://microg.org/)! microG is helping us to get rid of the Google services. It needs good developers to help and accelerate the development and fix issues. Looking for a first developer challenge with microG? Help fixing issues and contribute [on GitHub](https://github.com/microg/).
- Join the test team, get the latest builds and help us hunt all bugs! [Apply from here](https://doc.e.foundation/testers).
- Help us top fix issues! Reporting issues is great for Murena, fixing them **deserves our eternal recognition**. Want to [pick an issue and work on it](https://gitlab.e.foundation/groups/e/-/issues)?
- OpenCamera: some issues have been detected on some devices, probably related to some hardware drivers/firmware. For instance, OpenCamera is randomly crashing, depending on some settings. Also, we’d like to have the best ever open-source camera application in term of usability and picture result. If interested in this project please contact us, and we will start a fork (+ contribute upstream). [contact us](https://doc.e.foundation/projects-looking-for-contributors#how-to-contact-us)!
- Personal assistant: this is going to be a key project for Murena: we need a personal assistant that respects your privacy. The project has started with a first PoC! We need ASR specialists, conversational models specialists, AI specialists (deep learning/neural networks). Check [elivia](https://gitlab.e.foundation/e/elivia)! it’s our first proposal of Personal assistant… If interested in joining, please [contact us](https://doc.e.foundation/projects-looking-for-contributors#how-to-contact-us) about it.
- ROM developer/maintainer for Qualcomm (Snapdragon) and Mediatek chipsets: we want to provide support for newest devices available on the market. If you are interested in working on this, please go through the details [here](https://doc.e.foundation/rom-maintainer).

## Major /e/OS-related projects looking for contributors

/e/OS is using, modifying, integrating a lot of different open source software components, including those ones that are key for /e/OS:

- LineageOS: /e/OS is forking LineageOS for different needs, different users. Therefore, better hardware support for LineageOS means a better hardware support for /e/OS. [Contribute to LineageOS](https://lineageos.org/)!
- Searx: searx is a great open source meta-search engine that we are forking to make our default search engine ([spot.murena.io](https://spot.murena.io)). [Contribute to Searx](https://asciimoo.github.io/searx/)!
- Exodus Privacy: Exodus Privacy is a non-profit organization that creates software tools to inform people about privacy issues in Android applications, such as abusive permissions &amp; trackers. They [welcome software development and financial](https://exodus-privacy.eu.org/) contributions!
- NextCloud: several Murena online services heavily rely on NextCloud (a fork of OwnCloud), like cloud storage, online calendar, notes etc. [Contribute to NextCloud](https://nextcloud.com/contribute/)!

## Is it paid?

Projects here are open source projects and we know that many people really appreciate voluntary contributions to open source projects, for the benefit of all.

However, if you think, as a specialist, that the project will require a huge amount of work and that you are ready to make it within a limited timeframe, we can consider a bounty or a paid-project. Please contact us.

## How to contact us?

Please send an email to <join@murena.io>, with a clear subject line (like: OpenCamera development ).

Tell us why you are interested in participating, how your skills are related to this project, and any other useful information.
