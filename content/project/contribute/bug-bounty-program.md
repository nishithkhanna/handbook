---
weight: 5
---

# Bug bounty program

### 📰 Purpose

e Foundation, which started in 2017 targets to support the maximum number of devices possible. The bug bounty program covers a set of pending tasks in our [backlog](https://gitlab.e.foundation/e/backlog/-/issues?scope=all&state=opened&label_name[]=bug%20bounty%3A%3A*) that we want to handle for our community and users. In order to focus on the user experience and stability of all devices we would like to solve some issues. More often minor but enough for impacting the global reliability. With this program the e Foundation wants to reward contributors for their work and improving the /e/OS deGoogled and pro-privacy mobile OS user experience for the masses!

### 💰 Reward

Each issue linked with the program is tagged with a scope label `bug bounty` and a price in €uro.

An [Open Collective](https://opencollective.com/e_foundation) account centralizes all transactions, for getting your reward you need to have:

- An Open Collective account
- A bank account

📣 If you fix a set of 3 issues in 3 months, we will double your total reward! 💶 💶

### 📐 Rules

#### Workflow

- Send an email to <bugbounty@murena.io> to join the program with the details below: 
    - [/e/OS Gitlab username](https://gitlab.e.foundation/e)
    - Telegram username
- Find an issue to fix in our [backlog](https://gitlab.e.foundation/e/backlog/-/issues?scope=all&state=opened&label_name[]=bug%20bounty%3A%3A*)
- Assign the selected issue to you (automatically unassigned after 2 weeks of inactivity)
- Fork the related project
- Submit a Merge Request on the main project, which have to follow: 
    - Add a reference to the Gitlab issue in the Merge Request description
    - Respect coding guidelines of the project
    - Have a test case to valid your work
    - Fix the issue
- Add a message about your contribution in the telegram group

#### Guidelines

- All communications with the /e/OS team have to be done on telegram or in the merge request.
- The /e/OS team reviews merge requests by submission date order. If a merge request is reviewed and not handled by the author after 30 days the /e/OS team will be allowed to continue or close the work done. Unfortunately no reward could be granted.
- This program covers only the bugs from our backlog tagged with the scope label `bug bounty`, if you think that some are relevant for the project you can argue on the telegram group for including it in the bug bounty program. For example if an issue contains a lot of thumbs up, a lot of expectation from the community or if the impact is really high for the user experience, this one can be easily handled in this program after the /e/OS team confirmation.

### 🤝 Engagement

- The /e/OS team will review and validate your work in less than 10 working days after your submission.
- The payout process starts once your work is merged.

Interested in our bug bounty program? Pick an [issue](https://gitlab.e.foundation/e/backlog/-/issues?scope=all&state=opened&label_name[]=bug%20bounty%3A%3A*) now!