---
weight: 2
---
# ROM Maintainer

Before we get to the part where you apply to become a ROM Maintainer, lets first answer some of the frequently asked questions on this subject.

## Who is a ROM Maintainer?

A simple definition for the term `ROM Maintainer` is a user who builds the ROM that is flashed on a particular device. The ROM maintainers are responsible for the issues users face while flashing or using the ROM.

- This definition will change in the context of /e/OS.. more on that later in this document

## Do you have ROM Maintainers in /e/OS ?

Yes we do have ROM Maintainers who build for specific devices

## Do you have ROM Maintainers for all devices on your supported list?

No. For devices where we do not have a dedicated ROM Maintainer we build using our automated build infrastructure

## Are there any differences between the ROM Maintainers of other custom ROM’s and /e/OS?

Yes and these differences are quite important

- /e/OS ROM Maintainers are in charge of their device specific bugs.
- /e/OS ROM Maintainer names are not disclosed or shared with other users. This is done to respect the privacy of the ROM Maintainers unless they personally want to share their details with users.

## What are the ROM Maintainers’s responsibilites

The ROM Maintainer are expected to do the following

- Build the /e/OS ROM using the same code base as available on the [/e/OS Gitlab](https://gitlab.e.foundation/e/os/android)
- If the device is not supported by LineageOS this would mean the ROM Maintainer will have to port the device trees and sync them with /e/OS source code
- There will be no changes of any kind made to the ROM or the source code as it is shared on the [/e/OS Gitlab](https://gitlab.e.foundation/e/os/android)
- This specifically means that the same set of applications available on the [/e/OS Gitlab](https://gitlab.e.foundation/e/os/android) will be used in the build.
- The ROM Maintainer has to first test the ROM on their own device, ensure it is working correctly and then release it for all users
- If any device specific issues come up during the testing the device maintainer is expected to resolve those issues before sharing the final working build with the users.

## What is the process to become a ROM Maintainer?

### Prerequisites

If you are interested in becoming a ROM Maintainer it is assumed that you have ample experience of building and flashing custom ROM’s. We also assume that you are an expert at debugging issues encountered during the build process.

### Steps

1. First you should build an unofficial /e/OS ROM using the same code as available on our [/e/OS Gitlab](https://gitlab.e.foundation/e/os/android) You are free to choose the build mode you prefer. You can either use Docker or the traditional repo sync method to build the ROM.
2. Flash it on your own device and test it. Check that the device is functioning as expected. Which basically means you are able to make and receive calls, send and receive text messages …
3. Share this unofficial build with users on [our forum](https://community.e.foundation/c/e-devices/unofficial-builds/92). Create a new topic for this and mark it as an unofficial build.
4. Once the users test the build they will share their feedback on the same
5. Based on a reasonable number of positive feedback you can send us a mail to <join@murena.io>, with a clear subject line (like: ROM Maintainer Mediatek devicename) and we will take it up from there.

## Do you have a separate group or channel for ROM Maintainers

Yes we have a separate telegram channel exclusive for ROM Maintainers. Once you are a part of the ROM Maintainers your credentials will be added to this group.

## Can I withdraw from the ROM Maintainers groups?

This is a voluntary effort so Yes you can withdraw from being a ROM Maintainer at any point of time. Just let us know in advance so that we can make alternate arrangements for the builds to be available for users.

## What to do if you still have more questions

If you have any further doubts please send in a mail to <helpdesk@murena.com> and we will get back to you at the earliest possible.
