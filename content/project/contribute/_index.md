---
linkTitle: "Contribute"
cascade:
  type: docs
weight: 1
---

## Table of contents

- [Looking for contributors](looking-for-contributors)
- [Rom maintainer](rom-maintainer)
- [Translators](translators)
- [Testing](testing)
- [Bug bounty program](bug-bounty-program)
