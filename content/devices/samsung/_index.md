---
title: Samsung
weight: 3
---

## Samsung

| Brand | Name | /e/OS Version | Apps Compatibility | Available Through | Release year | Removable Battery |
| ----- | ---- | ------------- | ------------------ | ----------------- | ------------ | ----------------- |
| Samsung | [Galaxy S9](samsung/samsung-galaxy-s9-starlte) | T Official | | | 2021 | YES |
