---
linkTitle: S9 (starlte)
---

## Install /e/OS on Samsung Galaxy S9 starlte

The instructions in this guide will assist you to install /e/OS on your starlte. Please read through the instructions at least once before actually following them, so as to avoid any problems later.

It is important that you know that downgrading Smartphones on OS versions greater than the /e/OS OS version you plan to install, can cause instability or at worst brick some devices. We recommend that you check your current OS version, before attempting the installation.

It is advisable to flash your device only if you **know** what you are doing and are ok taking the **associated risk**. All /e/OS builds are provided as best effort, **without any guarantee**. The /e/OS project and its project members deny any and all responsibility about the consequences of using /e/OS software and or /e/OS services.

# Requirements

- If required take a backup of all important data from your phone on an external storage device before proceeding.
- Do not take a backup on the same device as some of these actions will format the device and delete the backup.
- Ensure your phone is charged more than 50%
- Check that `adb` is enabled on your PC. If not you can find the setup instructions [here](https://wiki.e.foundation/pages/install-adb)
- Download all the files and images mentioned in the download section below before starting the installation
- Make sure you have a working data cable to connect your device to the PC. There are cables which are only for charging and do not transfer data.
- Enable USB debugging on your device. You can find the instructions [here](https://wiki.e.foundation/pages/enable-usb-debugging)
- Make sure that your model is listed in the [Smartphone Selector](https://doc.e.foundation/devices). Check the model supported. Where available, information on the model supported should show as a pop-up when you hover the cursor on the Device name. The model number supported should be the exact same.
- Boot your device with the stock OS at least once and check every functionality.

{{< callout type="warning" >}}
Make sure that you can send and receive SMS and place and receive calls (also via WiFi and LTE, if available), otherwise it will not work on /e/OS as well. Additionally, some devices require that VoLTE/VoWiFi be utilized once on stock to provision IMS.
{{< /callout >}}

Please share your experience, suggest tips or changes to this install guide documentation by visiting the Galaxy S9 specific [topic on our community forum](https://community.e.foundation/t/samsung-galaxy-s9-starlte-documentation-suggestions/21032)

To report issues in /e/OS please refer [this guide](https://wiki.e.foundation/support-topics/report-an-issue)

## Downloads for starlte

- [/e/OS Recovery for community build](https://images.ecloud.global/dev/starlte)
- /e/OS build : [T community](https://images.ecloud.global/dev/starlte)
- /e/OS build : [S official](https://images.ecloud.global/stable/starlte)

> To understand the difference between /e/OS builds check [this guide](https://wiki.e.foundation/build-status)

- Before following these instructions please ensure that the device is on the latest **Android 10** firmware.

## Preparing for installation using Heimdall

Samsung devices come with a unique boot mode called “Download mode”, which is very similar to “Fastboot mode” on some devices with unlocked bootloaders. Heimdall is a cross-platform, open-source tool for interfacing with Download mode on Samsung devices. The preferred method of installing a custom recovery is through this boot mode – rooting the stock firmware is neither necessary nor required.

Please note: On windows PC’s to run the installation using ODIN check this [HOWTO](https://community.e.foundation/t/howto-install-e-on-a-samsung-smartphone-with-windows-easily/3186). The steps given below explain how to install Heimdall and flash /e/OS

1. Enable OEM unlock in the Developer options under device Settings. 
> It appears sometime that the OEM unlock option is missing from Development options, especially on new devices. Please connect your device to the wifi and check of system updates. After a reboot, the option should appear.
2. [Install Heimdall](https://wiki.e.foundation/support-topics/install-heimdall) or if you prefer you can [Build latest Heimdall suite](https://wiki.e.foundation/support-topics/build-heimdall)
3. Power off the device and connect the USB adapter to the computer (but not to the device, yet).
4. Boot into download mode:
    
    
    - hold `Volume Down` + `Bixby` + `Power`.
    
    You must accept/continue with the warning in “Download mode” to unlock the bootloader (see image below). You can accept/continue by short-pressing the `Volume UP` button. Then, connect your device with your PC if you have already not done it. ![](https://wiki.e.foundation/images/download-mode.png)
5. Windows only: install the drivers. A more complete set of instructions can be found in the ZAdiag user guide.
    
    
    1. Run `zadiag.exe` from the Drivers folder of the Heimdall suite.
    2. Choose Options » List all devices from the menu.
    3. Select Samsung USB Composite Device or MSM8x60 or Gadget Serial or Device Name from the drop down menu. (If nothing relevant appears, try uninstalling any Samsung related Windows software, like Samsung Windows drivers and/or Kies).
    4. Click Replace Driver (having to select Install Driver from the drop down list built into the
    5. If you are prompted with a warning that the installer is unable to verify the publisher of the driver, select Install this driver anyway. You may receive two more prompts about security. Select the options that allow you to carry on.
6. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:
    
    ```
     heimdall print-pit
    
    
    ```
7. If the device reboots, Heimdall is installed and working properly.

## Installing the /e/OS Recovery on your starlte

1. Download the /e/OS recovery (linked in the Download section above)
2. Power off the device and connect the USB adapter to the computer (but not to the device, yet).
3. Boot into download mode:
    
    
    - With the device powered off
    - hold Volume Down + Bixby + Power.
    
    Accept the disclaimer, then insert the USB cable into the device.
4. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window in the directory the recovery image is located, and type: 
``` 
heimdall flash --RECOVERY recoveryfilename.img --no-reboot    
```
    
> Replace `recoveryfilename` with the name of the recovery you downloaded
    
5. A blue transfer bar will appear on the device showing the recovery being transferred.

{{< callout emoji="💡" >}}
  **Tip:** The device will continue to display `Downloading... Do not turn off target!!` even after the process is complete. When the status message in the top left of the device’s display reports that the process is complete, you may proceed.
{{< /callout >}}
    
6. Manually reboot into recovery:
    
    
    - With the device powered off, hold Volume Up + Bixby + Power. When the blue text appears, release the buttons.
    
    > On some devices, installation can fail if vendors are not up-to-date. In this case, please:
    > 
    > 
    > - Download [those vendors](https://androidfilehost.com/?fid=11410932744536982158)
    > - Install them 
    >     - On the device, go into `Advanced` &gt; `ADB Sideload`, then swipe to begin sideload
    >     - From the computer, please run `adb sideload <vendors file>`

> Note: Be sure to reboot into recovery immediately after having installed the custom recovery. Otherwise the custom recovery will be overwritten and the device will reboot (appearing as though your custom recovery failed to install).

> Note: Be sure to reboot into recovery immediately after having installed the custom recovery. Otherwise the custom recovery will be overwritten and the device will reboot (appearing as though your custom recovery failed to install).

## Steps to install /e/OS from recovery

{{< callout emoji="💡" >}}
  Use the volume keys to navigate and power key to select
{{< /callout >}}
{{< callout emoji="💡" >}}
  Use the volume up key to choose the arrow at the top to go back to main screen
{{< /callout >}}
{{< callout emoji="💡" >}}
  If your PC is unable to detect your device with adb in the /e/OS recovery main screen tap `Advanced` » `Enable adb`
{{< /callout >}}

## Format the device

On /e/OS Recovery main screen:

1. Select `Factory reset`
2. Select `Format data / Factory reset` option
3. Next screen will display a warning that this action cannot be undone
4. Select `Format data` to proceed or `Cancel` if you want to go back
5. If you selected `Format data`, the format process will complete

{{< callout emoji="💡" >}}
  
  You will see text in small font on the lower left side of the screen mentioning format progress
{{< /callout >}}
    
>The small font text would be similar to this 

```
Wiping data
Formatting/data
Formatting/cache
Data wipe complete    
```
6. Display will now return to the Factory Reset screen

## Install /e/OS

In /e/OS recovery main screen:

1. Select `Apply Update` and in next screen `Apply update from adb`
2. In the next screen, the device is now in sideload mode
    
    > Note at this point the Cancel option is highlighted that does not mean you have canceled the action. The device is in adb sideload mode.
3. On your PC type begin adb sideload. Type the below command in a console
    
    `adb sideload downloaded_file_name.zip`
    
    > Replace downloaded\_file\_name.zip with the name of the /e/OS file you downloaded in the previous section
4. Press `enter` key on the keyboard to start the sideloading The screen will show the progress percentage…This might pause at 47%
5. Give it some time
6. The PC console will now display `Total xfer: 1.00x`
7. The phone screen will now display some text with a message similar to &gt; `Script succeeded result was [1.000000]`
    
    This means that the install was successful.

## Reboot the device

In /e/OS recovery main screen:

1. Select `Reboot system now`&gt; The reboot process may take 5 - 10 minutes

{{< callout emoji="💡" >}}
  Congratulations !! Your phone should now be booting into /e/OS !!
{{< /callout >}}

{{% details title="To find some troubleshooting tips… click here" closed="true" %}}

### adb

- Getting a `Total xFer ':' 0.01x` message - Try to upload the /e/OS to internal storage and retry to install
- `adb ':' sideload connection failed ':' insufficient permissions for device` error - Try to execute the commands `adb kill-server` and `adb start-server` and retry
- Running `adb shell twrp install /sdcard/</e/OS zip package>` raise non-existent directory error - Try to check solution [here](https://android.stackexchange.com/questions/226561/twrp-failed-to-install-zip-for-lineage-16-samsung-galaxy-note-2-gt-n7100-t03g)
- Getting an `insufficient permissions` error - Try the solution given [here](https://wiki.e.foundation/pages/insufficient-permissions)
- When running "adb devices" in bash a device is shown as "unauthorized" - Try the solution given [here](https://wiki.e.foundation/pages/unauthorized-device)

### TWRP

- Errors while running TWRP version 3.4.x.x or above - Try downloading and using a previous TWRP version.. below 3.4.x.x
- In TWRP if this error shows up `failed to mount /preload` - Try in TWRP `wipe` screen in advanced menu select `preload` and `swipe to wipe`
- `/cache/recovery/xxx -> no such file or directory` error while flashing a ROM - On PC run `adb shell` in the console now create a folder called recovery for e.g. here the devicecode name is violet "violet :/cache# mkdir recovery"

### Bootloop

- Issue specific to S9 and S9+ - Please refer [this guide](https://community.e.foundation/t/howto-the-system-doesnt-boot-bootloop/28266)


{{% /details %}}





Please share your experience , suggest tips or changes to this install guide documentation by visiting the Galaxy S9 specific \[topic on our community forum\](https://community.e.foundation/t/samsung-galaxy-s9-starlte-documentation-suggestions/21032).

To report issues in /e/OS please refer [this guide](https://wiki.e.foundation/support-topics/report-an-issue)

*This documentation “Install /e/OS on a Samsung Galaxy S9 - “starlte”” is a derivative of [“install LineageOS on starlte”](https://wiki.lineageos.org/devices/starlte/install) by The LineageOS Project, used under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/).*