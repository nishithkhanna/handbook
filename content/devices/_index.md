---
linkTitle: "Devices"
title: Introduction
cascade:
  type: docs
---

## Supported devices
| Brand | Name | /e/OS Version | Apps Compatibility | Available Through | Release year | Removable Battery |
| ----- | ---- | ------------- | ------------------ | ----------------- | ------------ | ----------------- |
| Fairphone | [FP4](fairphone/fp4) | T Official | | | 2021 | YES |
| Fairphone | [FP5](fairphone/fp5) | T Official | | | 2023 | YES | 
| Google | [Pixel 5](google/pixel5_redfin) | T Official | | | 2021 | YES |
| Samsung | [Galaxy S9](samsung/s9_starlte) | T Official | | | 2021 | YES |