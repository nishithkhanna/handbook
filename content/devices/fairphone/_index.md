---
title: Fairphone
weight: 1
---

## Fairphone

| Brand | Name | /e/OS Version | Apps Compatibility | Available Through | Release year | Removable Battery |
| ----- | ---- | ------------- | ------------------ | ----------------- | ------------ | ----------------- |
| Fairphone | [FP4](fairphone/fp4) | T Official | | | 2021 | YES |
| Fairphone | [FP5](fairphone/fp5) | T Official | | | 2023 | YES | 
