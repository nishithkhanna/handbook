---
linkTitle: FP5
---
## Install /e/OS on FP5


The instructions in this guide will assist you to install /e/OS on your FP5. Please read through the instructions at least once before actually following them, so as to avoid any problems later.

It is important that you know that downgrading Smartphones on OS versions greater than the /e/OS OS version you plan to install, can cause instability or at worst brick some devices. We recommend that you check your current OS version, before attempting the installation.

It is advisable to flash your device only if you **know** what you are doing and are ok taking the **associated risk**. All /e/OS builds are provided as best effort, **without any guarantee**. The /e/OS project and its project members deny any and all responsibility about the consequences of using /e/OS software and or /e/OS services.

# Requirements

- If required take a backup of all important data from your phone on an external storage device before proceeding.
- Do not take a backup on the same device as some of these actions will format the device and delete the backup.
- Ensure your phone is charged more than 50%
- Check that `adb` and `fastboot` are enabled on your PC. If not you can find the setup instructions [here](https://wiki.e.foundation/pages/install-adb)
- Download all the files and images mentioned in the download section below before starting the installation
- Make sure you have a working data cable to connect your device to the PC. There are cables which are only for charging and do not transfer data.
- Your device requires a code to unlock the bootloader. Get the code [here](https://www.fairphone.com/en/bootloader-unlocking-code-for-fairphone-3/) before proceeding with the next steps
- Enable USB debugging on your device. You can find the instructions [here](https://wiki.e.foundation/pages/enable-usb-debugging)
- Make sure that your model is listed in the [Smartphone Selector](https://doc.e.foundation/devices). Check the model supported. Where available, information on the model supported should show as a pop-up when you hover the cursor on the Device name. The model number supported should be the exact same.
- Boot your device with the stock OS at least once and check every functionality.

{{< callout type="warning" >}}
Make sure that you can send and receive SMS and place and receive calls (also via WiFi and LTE, if available), otherwise it will not work on /e/OS as well. Additionally, some devices require that VoLTE/VoWiFi be utilized once on stock to provision IMS.
{{< /callout >}}

Please share your experience , suggest tips or changes to this install guide documentation by visiting the FP4 specific [topic on our community forum](https://community.e.foundation/t/fairphone-fp4-fp4-documentation-suggestions/37933).

To report issues in /e/OS please refer [this guide](https://doc.e.foundation/support-topics/report-an-issue).

{{< callout type="error" >}}
The FP5 comes with an anti-rollback feature. Google Android anti-roll back feature is supposedly a way to ensure you are running the latest software version, including the latest security patches. If you try installing a version of /e/OS based on a security patch that is older than the one on your device, you will brick your device. Click on `Details` below for detailed information.  

{{% details title="Details" closed="true" %}}

To check the security patch level on your phone with a locked bootloader, prior to installing /e/OS, open your phone `Settings` » `About Phone` » `Android Version` » `Android Security Patch Level`.Then compare it against the level of the security patch on the /e/OS build as visible in the Downloads for FP5 section below.  
  
**The following values control whether anti-rollback features are triggered on FP5:**  
  
\* Rollback protection errors trigger if you install an update whose version number is `LESS` than the rollback index’s value stored on device.  
\* The value of rollback index is `UPDATED` to match `ro.build.version.security_patch`’s value of the currently installed version, but only if the bootloader is `LOCKED`.  
\* The value of rollback index is `NOT` dependent on the currently installed `ANDROID VERSION`.  
\* The value of rollback index can `NEVER` be `DOWNGRADED`.  
\* Rollback protection errors are `FATAL` when the bootloader is `LOCKED`.  
\* Rollback protection errors are `IGNORED` when the bootloader is `UNLOCKED`.  
  
Here are some examples to help you understand how anti-rollback features work:  
  
**Example 1**  
\* Your FP5 with Google Android has a Security Patch Level saying June 5, 2022  
\* The /e/OS build available says: /e/OS build : R stable (Security patch: 2022-05-05)  
\* In this example, the /e/OS build has an older Security Patch level than the origin, so the anti-roll back protection will trigger, and you will brick your phone  
**Example 2**  
\* Your FP5 with Google Android has a Security Patch Level saying June 5, 2022.  
\* The /e/OS build available says: /e/OS build : R stable (Security patch: 2022-06-05)  
\* In this example, the /e/OS build has the same Security Patch level than the origin, so the anti-roll back protection will pass, and you will be able to install /e/OS with no issues.  
  
**Example 3**  
\* Your FP5 runs Google Android -R while /e/OS is now available based on AOSP -S.  
\* Your FP5 with Google Android has a Security Patch Level saying 2022-10-03 or October 3rd, 2022.  
\* The /e/OS build available says: /e/OS build : S stable (Security patch: 2022-06-05)  
\* In this example, the /e/OS build has an older Security Patch level than the origin, so the anti-roll back protection will trigger, even if the /e/OS version runs on a more recent version of AOSP. In this example, you will brick your phone.

{{% /details %}}

{{< /callout >}}


## Downloads for FP5

- /e/OS build : [T community](https://images.ecloud.global/dev/FP5) (Security patch: 2024-05-05)
- /e/OS build : [T stable](https://images.ecloud.global/stable/FP5) (Security patch: 2024-05-05)

> To understand the difference between /e/OS builds check [this guide](https://wiki.e.foundation/build-status)

{{< callout type="warning" >}}
Please note some of the above links can lead to external sites
{{< /callout >}}



## Unlocking the bootloader

{{< callout emoji="💡" >}}
  The steps given below only need to be run once per device. 
{{< /callout >}}

{{< callout type="warning" >}}
Unlocking the bootloader will erase all data on your device! Before proceeding, ensure the data you would like to retain is backed up to your PC and/or an online drive.  
Some vendors/manufacturers prevent the bootloader from being unlocked. Depending on where your device was acquired you may or may not be able to unlock the bootloader. To verify if your device is compatible please check the [devices list](https://doc.e.foundation/devices/).
{{< /callout >}}

1. Boot the device
2. Enable and connect Wifi
3. Enable Developer options
4. From developer options, enable OEM unlock 
    - Get the unlock code from [this site](https://www.fairphone.com/en/bootloader-unlocking-code-for-fairphone-3/) and enter it
5. Reboot on fastboot by running the command in the PC console `adb reboot bootloader`

{{< callout emoji="💡" >}}
  All the console commands are run on an adb enabled PC and in the `adb` console.
{{< /callout >}}
    
6. Ask for unlock with `fastboot flashing unlock`
7. Approve with volume + then power
    - The device proceed with a factory reset, and automatically reboots
8. Reboot again into bootloader
9. Unlock critical with `fastboot flashing unlock_critical`
10. Approve with volume + then power
    - The device proceeds with a factory reset, and automatically reboots

## Installing /e/OS

{{< callout type="info" >}}
For Windows users, we advise to use Git Bash. For more info about Downloads and Installation see [Official Documentation](https://git-scm.com/about)
{{< /callout >}}

{{< callout type="warning" >}}
The FP5 comes with an anti-rollback feature. Read the paragraph in Requirements section of this guide, before proceeding.
{{< /callout >}}

1. Unzip the archive
    
    ```
     unzip <fileyoudownloaded>
    
    ```
    
    > In the command above, replace `<fileyoudownloaded>` with path of the downloaded archive.
    
    > Alternatively, you can unzip the archive using a File Manager/File Explorer.
2. Confirm that extracted directory contains following content:
    
    
    - `bin-linux-x86` directory - which contains linux tools including fastboot and adb
    - `bin-msys` directory - which contains Windows tools including fastboot and adb
    - `.img` files - The `.img` files that are to be flashed onto the device.
    - `flash_FP5_factory.sh` - The installer script.
3. Boot into bootloader/fastboot mode.
    
    
    - Power OFF the device.
    - Hold `Power` + `Volume Down` button combo simultaneously.
4. Run the installer script on Terminal(Linux) or on Git Bash UI(Windows).
    
    
    - Make sure you are in the current directory which contains the `flash_FP5_factory.sh` file.
    - **Linux**
        - Right click -&gt; Open terminal here
        - Execute the following command:
        
        ```
          chmod +x flash_FP5_factory.sh && ./flash_FP5_factory.sh
        
        ```
    - **Windows**
        - Right click -&gt; Git Bash here
        - Execute the following command:
        
        ```
          chmod +x flash_FP5_factory.sh && ./flash_FP5_factory.sh
        
        ```
    
    The script will flash all required files and will wait for input at the last step. Proceed to locking the bootloader.

## Locking the Bootloader

{{< callout type="warning" >}}
**The FP5 comes with an anti-rollback feature. Read the paragraph in Requirements section of this guide, before proceeding.**
{{< /callout >}}

Once you have completed the above steps and before rebooting you can lock your Bootloader.

1. Lock critical partition with the following command 
    - `fastboot flashing lock_critical`
    - Approve with Volume + then power
2. Reboot your device into bootloader, and plug it to your computer
3. Lock the device with the following command 
    - `fastboot flashing lock`
    - Approve with Volume + then power

{{< callout emoji="💡" >}}
  Congratulations !! Your phone should now be booting into /e/OS !!
{{< /callout >}}

Please share your experience , suggest tips or changes to this install guide documentation by visiting the FP4 specific [topic on our community forum](https://community.e.foundation/t/fairphone-fp4-fp4-documentation-suggestions/37933).

To report issues in /e/OS please refer [this guide](https://doc.e.foundation/support-topics/report-an-issue)
