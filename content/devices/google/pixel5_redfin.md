---
linkTitle: Pixel5 (redfin)
---


## Install /e/OS on a Google Pixel 5 redfin

The instructions in this guide will assist you to install /e/OS on your redfin. Please read through the instructions at least once before actually following them, so as to avoid any problems later.

It is important that you know that downgrading Smartphones on OS versions greater than the /e/OS OS version you plan to install, can cause instability or at worst brick some devices. We recommend that you check your current OS version, before attempting the installation.

It is advisable to flash your device only if you **know** what you are doing and are ok taking the **associated risk**. All /e/OS builds are provided as best effort, **without any guarantee**. The /e/OS project and its project members deny any and all responsibility about the consequences of using /e/OS software and or /e/OS services.

## Requirements

- If required take a backup of all important data from your phone on an external storage device before proceeding.
- Do not take a backup on the same device as some of these actions will format the device and delete the backup.
- Ensure your phone is charged more than 50%
- Check that `adb` and `fastboot` are enabled on your PC. If not you can find the setup instructions [here](https://doc.e.foundation/pages/install-adb)
- Download all the files and images mentioned in the download section below before starting the installation
- Make sure you have a working data cable to connect your device to the PC. There are cables which are only for charging and do not transfer data.
- Enable USB debugging on your device. You can find the instructions [here](https://doc.e.foundation/pages/enable-usb-debugging)
- Make sure that your model is listed in the [Smartphone Selector](https://doc.e.foundation/devices). Check the model supported. Where available, information on the model supported should show as a pop-up when you hover the cursor on the Device name. The model number supported should be the exact same.
- Boot your device with the stock OS at least once and check every functionality.

{{< callout type="warning" >}}
**Warning:** Make sure that you can send and receive SMS and place and receive calls (also via WiFi and LTE, if available), otherwise it will not work on /e/OS as well. Additionally, some devices require that VoLTE/VoWiFi be utilized once on stock to provision IMS.
{{< /callout >}}

Please share your experience , suggest tips or changes to this install guide documentation by visiting the Pixel 5 specific [topic on our community forum](https://community.e.foundation/t/google-pixel-5-redfin-documentation-suggestions/37156).

To report issues in /e/OS please refer [this guide](https://doc.e.foundation/support-topics/report-an-issue)

## Downloads for redfin

- [/e/OS Recovery for community build](https://images.ecloud.global/dev/redfin)
- /e/OS build : [T community](https://images.ecloud.global/dev/redfin)
- /e/OS build : [T official](https://images.ecloud.global/stable/redfin)

> To understand the difference between /e/OS builds check [this guide](https://doc.e.foundation/build-status)

- Before following these instructions please ensure that the device is on the latest **Android 13.0.0** firmware.

## Unlocking the bootloader

{{< callout emoji="💡" >}}
  **Tip:** The steps given below only need to be run once per device.
{{< /callout >}}

{{< callout type="warning" >}}
**Warning:** Unlocking the bootloader will erase all data on your device! Before proceeding, ensure the data you would like to retain is backed up to your PC and/or an online drive
Some vendors/manufacturers prevent the bootloader from being unlocked. Depending on where your device was acquired you may or may not be able to unlock the bootloader. To verify if your device is compatible please check the [devices list](https://doc.e.foundation/devices/).
{{< /callout >}}

1. Enable OEM unlock in the Developer options under device Settings, if present.
2. Connect your device to your PC via USB.
3. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type: ```
     adb reboot bootloader
    
    ```
    
    You can also boot into fastboot mode via a key combination:
    
    
    - With the device powered off
    - hold Volume Down + Power
4. Once the device is in fastboot mode, verify your PC finds it by typing: ```
     fastboot devices
    
    ```
{{< callout emoji="💡" >}}
  **Tip:** If you see `no permissions fastboot` while on Linux or macOS, try running `fastboot` as root.
{{< /callout >}}
    
5. Now type the following command to unlock the bootloader:
    
    ```
     fastboot flashing unlock
    
    ```
6. If the device doesn’t automatically reboot, reboot it. It should now be unlocked.
7. Since the device resets completely, you will need to **re-enable USB debugging** to continue.

## Flashing additional partitions

{{< callout type="warning" >}}
**Warning:** This platform requires additional partitions to be flashed for recovery to work properly.The steps to flash the partitions are given below
{{< /callout >}}

{{< callout emoji="💡" >}}
  **Tip:** In case the partition files are not available in the /e/OS recovery zip, users flashing the `Android 13 or /e/OS T build`, can try and download them from the LineageOS wiki install page specific to this device.
{{< /callout >}}

1. First unzip the /e/OS Recovery file linked in the section marked `Downloads for redfin` . The following files would show up in the location where you unzipped the contents of the eRecovery zip file 
    - boot.img
    - dtbo.img
2. Next we manually reboot into bootloader or download mode 
    - With the device powered off
    - hold Volume Down + Power
3. Flash the downloaded image files to your device by typing (replace `<...>` with the actual filenames): 
```
    fastboot flash boot <boot>.img
    fastboot flash dtbo <dtbo>.img  
```

## Installing a custom recovery using `fastboot`

1. Before proceeding ensure you have downloaded the custom recovery from the link given in the Download section above
2. Connect your device to your PC via USB.
3. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type: 
    ```
     adb reboot bootloader
    ```
    
    You can also boot into fastboot mode via a key combination:
    
    
    - With the device powered off
    - hold `Volume Down` + `Power`
4. Once the device is in fastboot mode, verify your PC finds it by typing: 
    ```
    fastboot devices
    ```
    {{< callout emoji="💡" >}}
  **Tip:** If you see `no permissions fastboot` while on Linux or macOS, try running fastboot as root
{{< /callout >}}
{{< callout emoji="💡" >}}
  **Tip:** Some devices have buggy USB support while in bootloader mode. If fastboot hangs with no output when using commands such as `fastboot getvar` .. , `fastboot boot` …, `fastboot flash` … try a different USB port (preferably a USB Type-A 2.0 one) or a USB hub
{{< /callout >}}
5. Flash a recovery image onto your device by typing
    
    ```
     fastboot flash vendor_boot recoveryfilename.img
    
    ```
    
    > Replace the `recoveryfilename` with the name of the recovery image you downloaded in the previous section
6. Now reboot into recovery to verify the installation: 
    - With the device powered off
    - hold Volume Down + Power and choose Recovery Mode.

## Steps to install /e/OS from recovery

{{< callout emoji="💡" >}}
  **Tip:** Use the volume keys to navigate and power key to select
{{< /callout >}}
{{< callout emoji="💡" >}}
  **Tip:** Use the volume up key to choose the arrow at the top to go back to main screen
{{< /callout >}}
{{< callout emoji="💡" >}}
  **Tip:** If your PC is unable to detect your device with adb in the /e/OS recovery main screen tap `Advanced` » `Enable adb`
{{< /callout >}}

## Format the device

On /e/OS Recovery main screen:

1. Select `Factory reset`
2. Select `Format data / Factory reset` option
3. Next screen will display a warning that this action cannot be undone
4. Select `Format data` to proceed or `Cancel` if you want to go back
5. If you selected `Format data`, the format process will complete

{{< callout emoji="💡" >}}
  **Tip:** You will see text in small font on the lower left side of the screen mentioning format progress
{{< /callout >}}
    
> The small font text would be similar to this
    
```
Wiping data
Formatting/data
Formatting/cache
Data wipe complete
```
6. Display will now return to the Factory Reset screen

## Install /e/OS

In /e/OS recovery main screen:

1. Select `Apply Update` and in next screen `Apply update from adb`
2. In the next screen, the device is now in sideload mode
    
    > Note at this point the Cancel option is highlighted that does not mean you have canceled the action. The device is in adb sideload mode.
3. On your PC type begin adb sideload. Type the below command in a console
    
    `adb sideload downloaded_file_name.zip`
    
    > Replace downloaded\_file\_name.zip with the name of the /e/OS file you downloaded in the previous section
4. Press `enter` key on the keyboard to start the sideloading The screen will show the progress percentage…This might pause at 47%
5. Give it some time
6. The PC console will now display `Total xfer: 1.00x`
7. The phone screen will now display some text with a message similar to > `Script succeeded result was [1.000000]`
    
    This means that the install was successful.

### Locking the bootloader

In /e/OS recovery main screen:

1. Select `Advanced`
2. Select `Reboot to bootloader`

Once the device is in fastboot mode:

1. Verify your PC finds it by typing: 
    ```
     fastboot devices
    
    ```
    {{< callout emoji="💡" >}}
  **Tip:** If you see `no permissions fastboot` while on Linux or macOS, try running `fastboot` as root.
{{< /callout >}}
    

2. Download the [avb custom key](https://images.ecloud.global/stable/pkmd_pixel.bin)
3. Erase the previous key `fastboot erase avb_custom_key`
4. Flash the new key previously downloaded `fastboot flash avb_custom_key pkmd_pixel.bin`
5. Lock the device `fastboot flashing lock`
6. Use the volume key to select `Lock the bootloader`

## Reboot the device

1. Use power button to `Start` the device 
> The reboot process may take 5 - 10 minutes

### Disabling OEM Unlocking.

Once you boot your device after locking bootloader:

1. Finish `SetupWizard`
2. Go to Settings &gt; About phone &gt; Build number.
3. Tap the Build Number option seven times until you see the message You are now a developer! This enables developer options on your device.
4. Return to the previous screen and go to `System` to find Developer options at the bottom.
5. In `Developer Options`, turn `OEM Unlocking` off.

{{< callout emoji="💡" >}}
  **Success:** Congratulations !! Your phone should now be booting into /e/OS !!
{{< /callout >}}


{{% details title="To find some troubleshooting tips… click here" closed="true" %}}

### adb

- Getting a `Total xFer ':'  0.01x` message - Try to upload the /e/OS to internal storage and retry to install
- `adb ':' sideload connection failed ':' insufficient permissions for device` error - Try to execute the commands `adb kill-server` and `adb start-server` and retry
- Running `adb shell twrp install /sdcard/</e/OS zip package>` raise non-existent directory error - Try to check solution [here](https://android.stackexchange.com/questions/226561/twrp-failed-to-install-zip-for-lineage-16-samsung-galaxy-note-2-gt-n7100-t03g)
- Getting an `insufficient permissions` error - Try the solution given [here](https://doc.e.foundation/pages/insufficient-permissions)
- When running "adb devices" in bash a device is shown as "unauthorized" - Try the solution given [here](https://doc.e.foundation/pages/unauthorized-device)

### TWRP

- Errors while running TWRP version 3.4.x.x or above - Try downloading and using a previous TWRP version.. below 3.4.x.x
- In TWRP if this error shows up `failed to mount /preload` - Try in TWRP `wipe` screen in advanced menu select `preload` and `swipe to wipe`
- `/cache/recovery/xxx -> no such file or directory` error while flashing a ROM - On PC run `adb shell` in the console now create a folder called recovery for e.g. here the devicecode name is violet "violet :/cache# mkdir recovery"

### Connectivity

- Trying to troubleshoot mobile connectivity issues - Read this [guide](https://doc.e.foundation/pages/troubleshooting-mobile-connectivity)


{{% /details %}}
