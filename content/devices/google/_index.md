---
title: Google
weight: 2
---

## Google

| Brand | Name | /e/OS Version | Apps Compatibility | Available Through | Release year | Removable Battery |
| ----- | ---- | ------------- | ------------------ | ----------------- | ------------ | ----------------- |
| Google | [Pixel 5](google/pixel5) | T Official | | | 2021 | YES |
