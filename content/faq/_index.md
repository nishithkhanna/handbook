---
linkTitle: "FAQ"
cascade:
  type: docs
sidebar:
  exclude: true
---

## Table of contents

- [App Lounge](app_lounge)
- [Easy installer](easy_installer)
- [Applications](applications)