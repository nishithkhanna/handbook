---
weight: 2
---

## What does the Easy Installer do?

The Easy Installer is a software that will guide you as you flash /e/OS on your phone all by yourself. Unfortunately, you can’t just plug in your phone and wait until the process is done. There is always some manual action required. The Easy Installer will tell you what and when you have to do something. For example: pressing one or several physical buttons, taping on some buttons, enabling options, rebooting your phone, etc.

It will also perform commands in the background that you would otherwise have to run yourself.

Within a shell…(you know, this scary black screen window where geeks write strange things :computer:)

![Linux shell prompt example](https://doc.e.foundation/images/Shell2.png)

In addition, it will download source files and check their integrity.

Please do not rush, and read all the instructions carefully. They aren’t perfect yet, and are still being improved.

## My phone isn’t detected

### Windows users: You need drivers for your phone.

This is a complicated subject, but you have to know that there isn’t one single driver that works for everything.

A driver is a piece of software that allows your Windows computer to communicate with a plugged phone. There are drivers for your screen, mouse, keyboard, printer, gamepad, smartphone, etc. Unfortunately, some drivers are hard to find and don’t work with all smartphone modes.

{{< callout emoji="💡" >}}
  **Tip:** To install a missing fastboot driver, your phone must be in Fastboot mode
{{< /callout >}}

## FP3/FP3+

### Solution 1: Using Windows update

1. Go to Windows Update 
    - Click on the Start menu in the lower left corner.
    - Click on `Settings`
    - Go to `Update and Security`
    
    ![screenshot of windows update menu](https://doc.e.foundation/images/windows-update-menu.png)
2. Install the necessary driver 
    - Click on `Search for updates`
    
    ![screenshot of windows looking for update](https://doc.e.foundation/images/windows-searchForUpdate.png)
3. A new section named `Show optional updates` should appear. Click on it.
    
    ![Scrrenshot of window optionnal update menu](https://doc.e.foundation/images/windows-showOptionnalUpdate.png)
4. Click on `Driver updates` to display the list.
    
    ![screenshot of windows drivers update menu](https://doc.e.foundation/images/windows-clickOnDriverUpdates.png)
5. Select the driver named `Google, Inc. - Other hardware - Android Bootloader Interface` and click on `Download` and install.

{{< callout type="info" >}}
**Tip:** If the driver does not work, there are other drivers listed. Feel free to install and try them.
{{< /callout >}}

![Screenshot of driver selection of 'Google, Inc. - Other hardware - Android Bootloader Interface'](https://doc.e.foundation/images/windows-selectDriverToUpdate.png)


If the previous solution does not work, you can try this one:

### Solution 2: Manual installation of fastboot driver for FP3/FP3+

1. Download the driver. Unfortunately, Fairphone does not provide an official “fastboot interface driver”. You can use [the one we extracted](https://images.ecloud.global/tools/drivers/windows/fastboot) but there can be cases where it may not work.
2. Start your phone in Fastboot mode (turn it off then keep pressing Power + Volume down until it start in fastboot mode)
3. Plug your phone to your windows computer
4. Open device manager from windows settings
5. Select the android device with the warning icon
6. Right click to open details of the device
7. Move to Driver section, and select install/update driver
8. A message prompt will open. Choose to use local file
9. Select the folder where you downloaded the driver
10. Wait for the driver to install

{{< callout type="info" >}}
**Tip:** You can follow the [official android documentation about driver installation](https://developer.android.com/studio/run/oem-usb)
{{< /callout >}}

{{< callout type="info" >}}
**Tip:** You may have to reboot your computer after driver installation, to make them available.
{{< /callout >}}

## GS290

### Solution 1: Using Windows update

<div class="post-content" id="bkmrk-tip%3A-your-phone-must"><div class="alert alert-info"><div class="d-inline ml-1">**Tip:** Your phone must be in Fastboot mode, if you miss the fastboot interface driver</div></div>1. Go to Windows Update 
    - Click on the Start menu in the lower left corner.
    - Click on “Settings”.
    - Go to “Update and Security”.
    
    ![screenshot of windows update menu](https://doc.e.foundation/images/windows-update-menu.png)
2. Install the necessary driver 
    - Click on “Search for updates”.
    
    ![screenshot of windows looking for update](https://doc.e.foundation/images/windows-searchForUpdate.png)

- A new section named “Show optional updates” should appear. Click on it.

</div>![Scrrenshot of window optionnal update menu](https://doc.e.foundation/images/windows-showOptionnalUpdate.png)

<div class="post-content" id="bkmrk-click-on-%E2%80%9Cdriver-upd">- Click on “Driver updates” to display the list.

</div>![screenshot of windows drivers update menu](https://doc.e.foundation/images/windows-clickOnDriverUpdates.png)

<div class="post-content" id="bkmrk-select-the-driver-na">- Select the driver named “MediaTek - Other hardware - Android ADB Interface” and click on “Download and install. If it doesn’t work and you have other drivers listed, feel free to install them too.

</div>![Screenshot of driver selection of 'MediaTek - Other hardware - Android ADB Interface'](https://doc.e.foundation/images/windows-selectDriverToUpdate-GS290.png)

[Source from community forum](https://community.e.foundation/t/howto-gs290-stuck-in-fastboot-bootloader-mode-with-the-easy-installer-on-windows/29037)

### Solution 2: Manual installation of fastboot drivers for the GS290

<div class="post-content" id="bkmrk-download-the-driver.-1">1. Download the driver. Unfortunately, Gigaset does not provide an official “fastboot interface driver”. You can use [the one we extracted](https://images.ecloud.global/tools/drivers/windows/fastboot), but there can be cases where it may work.
2. Start your phone in Fastboot mode (turn it off then keep pressing Power + Volume up. Then select Fastboot with Volume up, and confirm with Volume down)
3. Plug your phone to your windows computer
4. Open device manager from windows settings
5. Select the android device with the warning icon
6. Right click to open details of the device
7. Move to Driver section, and select install/update driver
8. A message prompt will open. Choose to use local file.
9. Select the folder where you downloaded the driver
10. Then wait for the driver to install.

<div class="alert alert-info"><div class="d-inline ml-1">**Tip:** You can follow the [official android documentation about driver installation](https://developer.android.com/studio/run/oem-usb0)</div></div><div class="alert alert-info"><div class="d-inline ml-1">**Tip:** You may have to reboot your computer after driver installation, to make them available.</div></div></div>## Samsung phones

For Samsung phone, there is a external tool called “wdi-simple.exe” provided with easy-installer. It will be called during the process when required and install the driver your need to work with heimdall.

For curious, this tool is our build of [libwdi project](https://github.com/pbatard/libwdi).

## Teracube 2e

Information is coming… Meanwhile you can try same procedure as for GS290

### The phone asks me to allow USB debugging. What should I do?

The Easy Installer communicates with your phone thanks to a tool called ADB (Android Debug Bridge). When you use it for the first time, on a specific computer, a prompt message is displayed on your phone.

![Example of RSA fingerprint message](https://doc.e.foundation/images/Easy-installer-rsaFingerprint.png)

If you’re on a trusted computer, you should also tick the checkbox to always allow connections between the computer and the device. If you do not tick the checkbox, you will have to accept it again next time. You must accept this message by clicking on the “Allow” button.

#### I cancelled the message on my phone

If you clicked on the cancel button, then your phone can’t communicate with your PC. Just unplug then replug your phone, the message will show up again.

### Try another cable

You should try to use a different USB cable. Only use a cable that allows data transfer. Some cables are only meant for battery charging. USB cables are the source of many issues, because they’re often not made to last.

### Try another computer

If any of the above solutions worked, you should try with another computer

## Downloading of sources failed immediatly after the beginning

[Check that “images” server is available](https://status.e.foundation). If server is unavailable, please try again later.

## Downloading of sources failed after a long period

Click on the restart button displayed, then the downloading will restart from where it stopped. The sources server has a timeout set to 1 hours. Consequently, if your connection is slow, it might stop before the end.

## Instructions and illustrations don’t correspond to my phone

Pictures are here to help but there aren’t the main content. If you must choose, please follow the text intructions.

You’ll find help from [the community forum](https://community.e.foundation/c/community/easy-installer/136)

Help to improve instruction by making suggestion, translating or providing screenshots of your phone would be appreciate a lot!

## I want to select dev or stable builds

At the moment, you can only get the stable build, but this is definitively a feature we’d like to add.

## I do not understand English, I want the Easy Installer in my language

Easy-installer use your computer’s language. If this language isn’t supported, it will use english by default.

Easy-installer supports (partially sometimes) the following languages:

<div class="post-content" id="bkmrk-english-french-germa">- English
- French
- German
- Dutch
- Russian
- Basque

</div>## GS290 flashing does not finalize

We don’t have all the card in hand yet, but we’re investigating into this issue to bring a solution as soon as possible. Please share logs with us, and join [this discussion](https://community.e.foundation/t/gs290-easy-installer-stops/27917) on our community forum.

## “OEM unlock” option is missing in settings of my Samsung phone

The Samsung process to enable to activate “oem unlock” option in developer settings is quite complicated because they want to block the bootloader. Consequently, you won’t find official documentation about it.

However, you can find many links on internet on how to enable the “OEM unlock” option.

Community members also suggested:

<div class="post-content" id="bkmrk-https%3A%2F%2Fwww.xda-deve">- https://www.xda-developers.com/fix-missing-oem-unlock-samsung-galaxy-s9-samsung-galaxy-s8-samsung-galaxy-note-8/
- https://androidmore.com/fix-missing-oem-unlock-toggle-samsung/

</div>There are already several discussions on our forum about this:

<div class="post-content" id="bkmrk-https%3A%2F%2Fcommunity.e.">- https://community.e.foundation/t/i-do-not-have-the-option-for-unlock-oem-in-developer-options/20222/2
- https://community.e.foundation/t/galaxy-s8-oem-unlock/28305
- https://community.e.foundation/t/unclear-about-instructions-unlock-oem/20875/9

</div>You might also want to do the following:

<div class="post-content" id="bkmrk-connect-your-phone-t">- Connect your phone to Wi-fi
- Update your device to the latest version of the original OS

</div>## The installation of “TWRP” (the recovery) fails

There is a “special” step during the process for Samsung phones. After the recovery is installed, you’ll have to leave the download mode (by pressing continuously on “Power”+”volume down”). As soon as the screen become black (like an off device) you must immediatly start into recovery (by pressing continuously on “Power”+”volume up”+”bixby”/”home”). Our tip is to only move the finger from the volume button. The manipulation is a little tricky but not impossible.

**Note:** We know that there is no warning about it yet, but we’ll add it in future release.

You can find more information on the community forum :

<div class="post-content" id="bkmrk-https%3A%2F%2Fcommunity.e.-1">- https://community.e.foundation/t/e-on-samsung-galaxy-s8-cant-reboot-into-twrp/21696
- https://community.e.foundation/t/e-easy-installer-samsung-galaxy-s7-no-teamwin-help-please/27887/3

</div>If it still blocked, and if you have a windows computer, you can try to flash TWRP (the recovery) by using [ODIN tools](https://samsungodin.com/) instead of heimdall. Once TWRP is installed, you can restart easy-installer and finish the process.

## Unlock code from Fairphone website doesn’t work

Your smartphone must have the latest version of Fairphone OS to make the oem unlock code to work. If you are still unable to unlock your Fairphone, please [contact Fairphone directly](https://www.fairphone.com/en/about/contact-us/)

## On which OS did you test the easy-installer ?

We developed the Easy-installer on Ubuntu (20.04) and Windows (10). It should work without any issue on Windows 7, 8, 10, Ubuntu, ElementaryOS and Linux Mint.

## I want to install the Easy Installer on a Windows without admin access

You will be able to install, but you won’t get a shorcut in your non-admin account.

You have some knowledge in NSIS (tools to create windows installer)? Feel free to give us an hand at

## I want to see log files

The installer creates a log file with a randomly generated name (e.g. 45761d08-711f-435a-881d-a236949654bb.log) that might contain useful information if something doesn’t work as expected.

Location of the log file:

<div class="post-content" id="bkmrk-linux%3A-%7E%2Fsnap%2Feasy-i">- **Linux:** ~/snap/easy-installer/common
- **Windows:** %LOCALAPPDATA%\\easy-installer

</div>The log file is too large to upload to the forum directly. If you report a problem, please paste the contents of the logfile to https://haste.tchncs.de/ and click the save icon there which will generate a link which you can then use here.

If the log is too large even to be pasted and you already have an Murena account, you can ZIP the log and upload it to your Murena account cloud storage.

## My device is not detected on Windows

The Easy Installer requires drivers to be installed on Windows OS for device detection. You can download vendor specific drivers [here](https://developer.android.com/studio/run/oem-usb). For Google Nexus devices check [this guide](https://developer.android.com/studio/run/win-usb). Please note both are external links.

## My question is not listed here

If you don’t find an answer to your question, a solution to your issue, please contact the [community support](https://t.me/joinchat/D1jNSmJHKo4u70ha). You can also check on the [community forum](https://community.e.foundation/c/community/easy-installer/) or directly ask for help there.

You should also check [this page](https://community.e.foundation/t/howto-troubleshoot-issues-with-the-easy-installer/23290) which contains many hints.