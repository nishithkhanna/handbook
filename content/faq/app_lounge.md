---
weight: 1
---

## Where do the applications in the App Lounge come from?

App Lounge can be used to install Native as well as Progressive Web Apps (PWAs) from a single interface. Apps are managed differently depending on their source. Applications from the Google Play Store are fetched using the Google Play API. Progressive Web Apps (PWAs) and Open Source Apps from F-Droid are fetched using the CleanAPK API (more info on the CleanAPK is covered below). App lounge allows you to filter apps by Open Source, PWAs, or just show all apps.

## Is App Lounge open source?

Yes, App Lounge is an open source app and its source code is available on [our Gitlab](https://gitlab.e.foundation/e/os/apps)

## Is x app available on App Lounge?

As previously said, App Lounge includes apps from Google Play Store and F-Droid. So, most apps on the market are available. A quick search in the App Lounge will get the job done.

## I can’t see x app the App Lounge?

In rare cases an app might be missing. You can request the app to be added to App Lounge using the issue feature on [our Gitlab](https://doc.e.foundation/support-topics/report-an-issue).

## App Lounge is designed to help you choose better apps

Each app features privacy score, user rating, permissions requested by apps, and trackers present (if any). As of now, user ratings are calculated using multiple factors but, we are planning to implement native user ratings in the future.

## How is the Privacy Score calculated?

To understand what the Privacy Score is and how it is calculated, please refer to [this guide](https://doc.e.foundation/privacy_score).

Ratings are based on a number of factors, which are detailed in the [privacy score section](https://doc.e.foundation/privacy_score).

## How are apps in the App Lounge verified to be ‘original’?

All apps in the App Lounge are checked using a PGP signature check or a checksum. In case you suspect an app is tampered with, we would request you to please report it at <heldesk@e.email> and we will take a look at what the case is, while keeping you informed.

### Can I use paid apps/in app purchases available with Google play store?

App Lounge supports Sign in with Google to install/update apps purchased from the Google Play Store, but it does not currently allow in-app purchases or new paid app purchases. You do have two options if you want to purchase an app.

Option One: Purchase the app from the developer’s website. This would allow you to avoid relying on a third party (such as Google) to prove ownership of an app purchase.

Option two: Use another phone that has the Google Play Store and purchase apps with a Google account. Back in /e/OS, use the same account in the App Lounge to install/update the purchased app.

We recommend option one, but if you choose option two, we recommend creating a new Google account to avoid account issues and also have some privacy.

## How can I trust the CleanAPK API (cleanapk.org)?

- Till date, the only issue we have seen have been some outdated packages. Nevertheless, we ask our users to report any issue with packages delivered by CleanAPK.
- We are working on a technical app verification solution that proves an APK has been unmodified from source. This is easy with packages that come from F-Droid because there is a signature, but it’s not for packages that come from Play Store.
- We are evaluating different options for apps in the future. That could be a new “store” where publishers could push their apps, an alternative to Google Play. We already support a large collection of PWAs in /e/OS.
- We are waiting for authorities to come out with more regulations on this. For instance in the EU, it is likely that Google and Apple will have to allow users to use alternative stores of applications.

## How did /e/OS learn about CleanAPK? Never heard of it before and it doesn’t look like they spend a lot of effort on advertising :)

People contact us all the time about new projects, and we decided to leverage CleanAPK APIs to access FDroids and PWAs catalogs to give the best possible user experience for the largest number of users.

## Where can I find more information about CleanApk?

You can browse for more details on their [website information page](https://info.cleanapk.org/)

## Will /e/OS and the App Lounge forever be linked with CleanApk?

No, we are working on removing the link to CleanApk. Details as to when it will be removed will be shared through our [newsletter](https://murena.com/about/) and on our [forum](https://community.e.foundation/)

## Progressive Web Apps (PWA)

Another option is to look for a web alternative. Several services are available with progressive web apps that can be used on the mobile: Uber, Twitter, Starbucks….

Some banks are also offering web services as an alternative to native apps.

For easier access, a shortcut to the web service can easily be added to the /e/OS BlissLauncher, using the web browser.