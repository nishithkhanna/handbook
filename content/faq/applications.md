# FAQ on available applications

## Purpose of the document

This guide explains how to control which applications are available on your Murena cloud account, disabling those that you don’t need and enabling those that provide functionality which is of interest to you. It also tries to cover some of the frequently asked question around app availability on Murena cloud.

## FAQ on available applications

### Which applications are currently available on Murena cloud?

- Files
- Web e-mail (Rainloop)
- Contacts
- Calendar
- Photos
- Notes (Nextcloud)
- Tasks
- Activity
- Carnet (alternative notes app)
- Deck (project planning board)
- Bookmarks
- News (RSS reader)

### Why don’t you offer all possible applications?

For different reasons:

- All need checking and sometimes tweaking. Some don’t work out of the box or require external components.
- Once we enable them, we commit to the app being available in the future, keeping it updated and offering support to our users.
- We need to consider security and performance impact of a given app if enabled for everyone.
- It would complicate the UI and usability of the cloud.

We [ran a survey](https://community.e.foundation/t/adding-new-apps-at-ecloud-global/12404/69) last year and we used the results to enable *Carnet*, *Deck*, *Bookmarks* and *News*.  
*Maps* and *Forms* need further polish before they can be made available.

### Can you enable application XYZ?

We will run similar polls from time to time. You can create a topic on the [Community Forum](https://community.e.foundation/) and see if other users agree with such app being useful. We will then notice popular topics and evaluate the proposal.

### Do premium subscribers have access to more or better apps?

Not for now, but this may change in the future. For instance, we may offer apps that have some higher hardware requirements (like streaming music), or a licensed limit on the number of users.

## Configuring application order

You can decide the order of the applications in the navigation by just draging and droping the apps icons like the video below:

<video controls="controls" height="402" style="width: 807px; height: 402px;" width="807"> <source src="https://doc.e.foundation/images/howtos/ecloud/reordering_applications.mp4" type="video/mp4"></source></video>