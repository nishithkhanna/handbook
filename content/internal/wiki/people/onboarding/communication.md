+++
title = 'Communication'
date = 2024-07-03T15:03:02+05:30
weight = 200
+++

* We promote as much as possible asynchronous communication. In our case, it means using GitLab issues and merge request in priority for communication, to ask or share something. Don't forget to tag the team member(s) interested your comment, so they will be notified!
* Issues: the list of issues assigned to you is the list of task you have to work on. It is visible at <https://gitlab.e.foundation/dashboard/issues>, using a filter with your GitLab username (shortcut on GitLab top bar, at right).
* Merge request (MR): we ask you to publish all your work as soon as you develop something in a MR. It's helpful for collaboration (easy to provide feedback), and also useful as backup (in case something goes wrong on your computer). Using the Assignee field in the MR, you can ping someone to review your work. A shortcut is available on GitLab top bar to see all the MR assigned to you. All the documentation is available at <https://gitlab.e.foundation/internal/wiki/-/wikis/development-process/merge-requests>
* MR to review: on a regular basis, kindly check the MR another dev asked you to review. A shortcut is available on GitLab top bar to see all the MR waiting for a review from you.
* To-Do list: it's a useful place where you can see all the notifications GitLab raised for you (tag in a comment, assignement to a new issue, asked for a MR review...). A shortcut is available on GitLab top bar to see you todo list.\
  ![image](uploads/519f89797fd5609257c946134534ffe9/image.png)
* If you need a quick answer, you can use Telegram. Kindly use as much as possible our team groups, so in case you share something interesting, every team member will be able to see it:
  * **/e/ developers**: the group with all /e/ developers. Useful to share info across all the team. The place to say "hello" when you start your day!
  * **/e/OS porting task force**: the group with device maintainers.
  * **/e/ Team status**: a channel dedicated to the weekly status. It's useful for the weekly team call on Monday. I will ask you, starting from this week, to share your status on this dedicated Telegram group. The goal is to list what you did during the week, what you plan to do for the next one and what are all the obstacles you were or you will face

* For some specific topics, it's sometime needed to have real time discussion. In that case, we can plan a call and go deeper in the subject.
* **1:1 call**: We will plan a time slot for our 1:1 call every 2 weeks. It's a time give to you to be able to share with us how do you feel, and find solution if something isn't good.
* **team call**: The purpose is to discuss together about difficulties any team member is facing. It's also a time useful to share info. You will get the invite once your manager gets your /e/ email address:
  * OS & device team weekly call is on Monday at 11:30 AM CET.
  * Cloud team weekly call is on Monday at 11:15 AM UTC.

* You will have to share an invoice at the end of the month to get paid. We created a documentation at <https://gitlab.e.foundation/internal/wiki/-/wikis/Contractor-invoice-process> to give you some inputs. Let us now if you need more info!

* Want to plan a call?
  * with 3 team members or less: you can use NextCloud functionnality "Create Talk room for this event" when planning the event in your calendar

![image](uploads/ce69ad2e3c1b9d91e125be3121eb342c/image.png)
  * with more than 3 team members: create a conference on [Jitsi Meet](https://meet.jit.si/)
  * get the e-mail address of your coworkers [here](https://gitlab.e.foundation/teams/team-map/-/boards/666).