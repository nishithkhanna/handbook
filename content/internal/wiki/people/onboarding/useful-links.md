+++
title = 'Useful Links'
date = 2024-07-03T15:03:39+05:30
weight = 200
+++

* Public website             
  * website: <https://e.foundation/>               
  * store: <https://murena.com/> 
  * user documentation: <https://doc.e.foundation/> 

* Developer links             
  * internal documentation: <https://gitlab.e.foundation/internal/wiki/wikis/home>               
  * development roadmap: <https://gitlab.e.foundation/groups/e/-/roadmap>               
  * milestones: <https://gitlab.e.foundation/groups/e/-/milestones>
  *  /e/ backlog: <https://gitlab.e.foundation/groups/e/-/boards/12>
  * translation tool: <https://i18n.e.foundation/>

* Help
  * glossary: <https://gitlab.e.foundation/internal/wiki/-/wikis/glossary>
  * How to use Helpdesk (Zammad): <https://user-docs.zammad.org/en/latest/>
  * teams: <https://gitlab.e.foundation/internal/wiki/-/wikis/teams/>


## Link to include in Cloud bookmark (to be discussed or updated)

**Platforms**
- https://gitlab.e.foundation/e [Sources codes, tickets]
- https://murena.io/ [Personal storage, with team's calendar, file sharing, mail, ...]
- https://murenatest.io/ [Test version of murena.io]
- https://community.e.foundation/ [Forum]
- https://i18n.e.foundation/ [Translation for Apps, site & docs]
- https://helpdesk.e.foundation/ [Tickets]

**Common and regular calls**
- https://meet.jit.si/e-devices-team-meeting
- https://meet.jit.si/e-apps-team-meeting

**Administrative link**
- https://gitlab.e.foundation/teams/team-map [Team organization plan]
- https://gitlab.e.foundation/internal/wiki/-/wikis/Paid-time-off

**Other**
- https://e.foundation/ [home site]
- https://gitlab.e.foundation/internal/wiki/-/wikis/
- https://gitlab.e.foundation/internal/wiki/-/wikis/glossary
- https://gitlab.e.foundation/groups/e/-/boards/12 [Issues board]
- https://forum.xda-developers.com/f/e-os-project.12553/ [Section on XDA]