+++
title = 'Glossary'
date = 2024-07-03T15:01:09+05:30
weight = 3
+++

> Feel free to contribute! Pay attention to keep the alphabetical order, and to validate the definition with another team member.

## 2
**2FA** Acronym for Two Factor Authentication

## A
**AAPT** Acronym for Android Asset Packaging Tool. It allows to view, create & update ZIP compatible archives (ZIP, JAR & APK). It can also compile resources into binaray assets. It is a base tool for building Android application.

**ABI** Application binary interface; The ABI defines exactly how your app's machine code is expected to interact with the system at runtime

**AID** Acronym for "Android ID"

**Android's Activity** In android development, it is an entry point, or a component, of an application designed to let the user interact with the application (by opposition to "Android Service").

**Android's Service** In android development, it is an entry point of an application, or a component, to perform some task in background (without user interaction).

**Android Vanilla** Standard android version shared by Google to manufacturer

**AndroidX** It is new package structure to make it clearer which packages are bundled with the Android operating system, and which are packaged with your app's APK. [source](https://stackoverflow.com/questions/51280090/what-is-androidx)

**ANR** Acronym for Application Not Responding [More here](https://stackoverflow.com/questions/20213482/what-is-the-difference-between-anr-and-crash-in-android)

**AOSP** Acronym for Android Open Source Project

**APN** Acronym for Access point Name. It is a set of datas relatives to a specific network of a specific network provider.[How to configure APN](http://www.frandroid.com/comment-faire/tutoriaux/351732_configurer-lapn-de-mobile-android)

**APK** Acronym for Application Package. It is the Package file format used by Android OS to distribute and install Apps and middleware.

**ARR** Acronym for Average Recurring Revenue

**Atomicity** Its a concept that represent a set of operation which can't be stopped

**AVB** Android verified boot. Check that the system isn't modified at boot.

**AVC** Access Vector Cache. It sores the access vector decisions made by the sepolicy engine of the linux kernel.[Debugging SE Android with messages](https://www.all-things-android.com/content/debugging-se-android-avc-messages)

## B
**baseband** this is the radio driver relative to telephone features.

**BCB** 
Android bootloader control block. It is a well established term/acronym in the Android namespace which refers to a location in a dedicated raw (i.e. FS-unaware) flash (e.g. eMMC) partition, usually called misc, which is used as media for exchanging messages between Android userspace (particularly recovery) and an Android-capable bootloader. [source](https://u-boot.readthedocs.io/en/latest/android/bcb.html)

**Bloatware** Software installed by default on Android

**BLOB** Acronym for Binary Large Object. It's a build-in type that stores a big object (example: picture or movie) as a column value in a row of a database table (in SQL context).


**BootLoader** This is the code executed before any OS starts. It packages the instructions to start the OS kernel. Most bootloaders have their own debugging and editing environment. 

## C
**CM** Acronym for "Cyanogen Mod". A custom ROM

**CSC** Acronym for "Country Exit Code". It's the languages and country-specific parameters.

**CWM** A older alternative to TWRP

**CTS** Acronym for Compatibility Test Suits. This is a set of test made to be integrated in CI process for manufacturer. Its goal is to reveal quickly incompatibility and to check that the OS stay compatible all along the process.

**CVE** Acronym for Common vulnerabilities and exposures system. It provides a reference-method for publicly known information-security vulnerabilities and exposures.

## D
**DAC** In SEPolicy context, it means Discretionary Access Control. It allows users to modify the permissions on files that they own while Linux root Account (UID 0) is able to modify the permissions on any file.

**dreamlte** Samsung Galaxy S8

**DRM** Digital Rights Management tools are a set of access control technologies for restricting the use of proprietary hardware and copyrighted works. DRM technologies try to control the use, modification, and distribution of copyrighted works (such as software and multimedia content), as well as systems within devices that enforce these policies.

## E
**Emulated storage** It corresponds to internal storage (or internal SD Card) of a smartphone.

**EI** In /e/ context, it refers to Easy-installer. A software that allow user to flash their devices themselves with a minimal technical knowledge.

**Exynos** This is a series of SoC, based on ARM architecture.

**E2E** Acronym for End to end. This represent a communication system where only communicators can read content (no backdoor or middle reader).

**E2EE** Acronym for End to End Encryption.

## F
**Firmware** is the operating software available on an Android device. It is composed of: CSC,  bootloader, PDA and Phone (the actual identifier of your device)

**FCM** Acronym for Firebase Cloud Messaging

**Flash** Process to change the OS of a device

**FoD** Acronym for "Fingerprint on display"

**FOSS** Acronym for Free and Open Source Software

**FS** In /e/ context, it refers to Flash-station project. It's the tool used to flash sold device.

**FTSW** Acronym for First Time Setup Wizard. It's the configurations interfaces of the device when you start it for the first time.

## H
**HAL** Acronym for "Hardware Abstraction Layer"

**herolte** Samsung Galaxy S7

**HMS** Acronym for Huawei Mobile Services

## I
**Idempotent** It's a concept that design an operation (in mathematics or computer science) that can be applied multiple times without changing the result beyond the initial application.


**Intent filter** in android development, this is a value which defines what can trigger a service or an activity. It is mostly defined in an file called "Manifest.xml"

**Interoperability** is a characteristic of a product or system, whose interfaces are completely understood, to work with other products or systems, at present or in the future, in either implementation or access, without any restrictions

## J
**JFS** Acronym for "Journal File System"

## K
**Kernel** This is the essential brick, present for all OS, which serves to make the communication between the software part and the hardware part.

**KG** Acronym for "Knox Guard"; Look at Samsung Knox. Possible status are: prenormal, competed, allzero, broken.

**klteaio** Samsung Galaxy S5 LTE

## L
**Launcher** It is a software used to locate and start other software easily. [src](https://en.wikipedia.org/wiki/Comparison_of_desktop_application_launchers)

**LOS** Acronym for LineageOS. It is a custom android ROM

**ls990** LG G3

**ls997** LG V20

## M
**MAC** in SEPolicy context, it means Mandatory Access Control

**.mk file** This is file for Android development used to bind C or C++ files to NDK

**MM** Acronym for Marshmallow. It's an android version

**MR** Acronym for merge request

**MVNO** Acronym for "Mobile virtual network operator"

**M2M** Acronym for "Machine to Machine"

## N
**NC** Acronym for Nextcloud

**NDK** Acronym for Native development Kit. It allow to compile native code as C or C++.

**NFS** Acronym for Network File System

**Nougat** Apart from the famous Montélimar specialty this name stands for Android 7.


## O
**OBB** Acronym for "Opaque binary Blob". It is a file format, developed by Google, for Android OS. It is used to package large files that are stored by apps in the public SD card of the user's Android device. They can only be decrypted and used by the Android apps that generated them.

**OEM** Acronym for "Original Equipment Manufacturer". This is the final Android version pushed on devices

**Orange state** Mechanism on some phone which indicate when the device is OEM unlocked.

**Oreo** Apart from the famous american biscuit this name stands for Android 8.

**OTA** Acronym for Over The Air [wiki](https://en.wikipedia.org/wiki/Over-the-air_programming). It refers to various methods of distributing new software, configuration settings, and even updating encryption keys to devices like cellphones, set-top boxes or secure voice communication equipment (encrypted 2-way radios)

## P
**PDA** Android operating system and your customizations.

**PIT file** Acronym for Partition Information Table. A PIT file is basically a set of instructions defining the phone partition layout (specifically for SAMSUNG Device). It is required to re-partition the phone with Odin software.

**Pie** Apart from a baked dish this name stands for Android 9.

## Q
**Q** Android 10

## R
**R** Android 11

**Radio** This is the lowest level in software stack. It runs before bootloader.

**RC** Acronym for Release candidate

**Recovery** It is a booting mode of Android device for backup & restoration. it can also be used to flash ROM, kernel, etc.

**Rescue party** Android 8.0 includes a feature that sends out a "rescue party" when it notices core system components stuck in crash loops. Rescue Party then escalates through a series of actions to recover the device. As a last resort, Rescue Party reboots the device into recovery mode and prompts the user to perform a factory reset. [more details](https://source.android.com/devices/tech/debug/rescue-party)

**RMM** Remote Monitoring and Management. The purpose of the function is to prevent the installation of individually developed or modified (rooted) Android systems, recovery, on the device, but the scope of Remote Monitoring comprises a number of other functions as well. The RMM function has been and is being introduced in an increasing number of Samsung upgrades since January 2018. Possible status are: normal, locked, blink, prenormal, completed.

**ROM** Acronym for "Read-only-memory". But in our context, it's rather a "dead-memory": It can we written only once.

**RRO** Acronym for "Runtime resource overlay". It's kind of apk that can change resources on the go.Let's say you have some color defined, red, and want to make it green. Using RRO, you just have to install the new RRO, and it will change the color without needing a reboot.


## S
**Samsung Knox** It is a security system made like an application which allow to separte personal data from work life data. [sources](https://www.samsung.com/fr/support/mobile-devices/qu-est-ce-que-samsung-knox-et-que-puis-je-faire-avec-cette-application/) & [good to read](https://chimeratool.com/en/docs/samsung-knox-guard-kg-and-remote-monitoring-and-management-rmm-unlock-functions)

**.so file** It is Native code (C or C++) compiled with NDK

**SoC** Acronym for System on a chipset. It's a full system embedded on a single chipset. It can contains memory, microprocessor and some peripheric.

**starlte** Samsung Galaxy S9

**star2lte** Samsung Galaxy S9+

**Stock ROM** The OS shipped originally with the device

## T
**Target Debugging** target debugging mostly allows developers to us Jdbg to do step by step on apps

**TB** Acronym for Thunderbird

**TWRP** Acronym for "Team Win Recovery Project". Open source project to restore image on Android device.


## V
**VCS** Acronym for Versioning Control System. It is a tool, or set of tool used to track evolution and update of project.

**VFS** Acronym for "Virtual File System"

**VNDK** Acronym for Vendor Native Dev Kit [more here](https://source.android.com/devices/architecture/vndk/abi-stability)

**VOSK** Speech recognition toolkit

**vs986_usu** LG G4

**VTS** Vendor Tests suits. It's automation for HAL & OS tests.

## X
**XAPK** It's a new Package file format for Android application (see APK). XAPK file = Single APK or multiple APKs (aka bundle) file + OBB data file.

**X2** phone producted by a canadian company called [Secure Group](https://securegroup.com/products/secure-hardware-platforms).