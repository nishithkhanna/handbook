+++
title = 'Time Off'
date = 2024-07-03T14:59:14+05:30
weight = 2
+++



## General

Time off is a given period of time,  where you are not asked to work. It means no development, no support but also no communication (via Telegram, mail, or GitLab issues). 

### How do I declare off days?

1. Kindly speak with your manager when you would like to take a moment off. Don't forget to mention the duration and the start date.
1. If your request has been approved, kindly report your off days in the /e/ team calendar

### When do I ask for off days?

The rule is: you have to ask for your off period in advance, a period equal to at least 4 times the off time you are requesting.

Kindly note it's the advance notice required before asking for off time. They will still need the approval by your manager before they are confirmed.

Examples:
- you want to be off a day, you have to ask at least 4 working days before
- you want to be off 2 days, you have to ask at least 8 working days before
- you want to be off a week, you have to ask at least 4 weeks before
