+++
title = 'Tools'
date = 2024-07-04T14:48:10+05:30
weight = 2
+++

## Softwere

- Telegram: install the app and ping your manager (for instance @rhunault, @aude)
- Murena cloud: create your account at <https://e.foundation/e-email-invite/> and send an e-mail at your manager (for instance [romain.hunault@e.email](mailto:romain.hunault@e.email), [arnauvp@e.email](mailto:arnauvp@e.email), [alexis.noetinger@e.email](mailto:alexis.noetinger@e.email)) to let him/her know your /e/ e-mail address
- GitLab: create an account at <https://gitlab.e.foundation/users/sign_in?redirect_to_referer=yes>, and forward your user ID to your manager and to Romain
    - [Backlogs](/tools/backlogs)
- Office: your manager will create your account once he/she gets your /e/ e-mail address
- device: you will need a device to work, let's discuss about that in the welcome call
- Jitsi: it is the tool used within the teams to make conference calls. Make sure your browser, camera and microphone are properly setup <https://meet.jit.si/> \
  _To easily create a meeting, just take the base URL_ [_https://meet.jit.si_](https://meet.jit.si/) _and add whatever you want (for instance_ [_https://meet.jit.si/e-onboarding-test_](https://meet.jit.si/e-onboarding-test)_) and share the URL with people you want to join the meeting._
- Weblate
- Community
- [Pluralsight](tools/pluralsight)
- [Kibana dashboard](tools/Kibana-dashboard-(elk-stack))

### Android applications

- Battery: [AccuBattery](https://play.google.com/store/apps/details?id=com.digibites.accubattery)
- Vibrations: [Vibetest](https://github.com/luk1337/VibeTest/releases/download/3452275363/app-debug.apk)
- Microphone and Speakers: [Fairphone Checkup](https://f-droid.org/fr/packages/community.fairphone.checkup/)
- Play service: [Play Services Info ](https://play.google.com/store/apps/details?id=com.weberdo.apps.serviceinfo)
- Play Integrity: [Play Integrity API Check](https://play.google.com/store/apps/details?id=gr.nikolasspyr.integritycheck)
- Safetynet: [SafetyNet Test](https://play.google.com/store/apps/details?id=org.freeandroidtools.safetynettest) or [YASNAC - SafetyNet Checker](https://play.google.com/store/apps/details?id=rikka.safetynetchecker&hl=en&gl=US)
- GPS: [SatStat](https://f-droid.org/fr/packages/com.vonglasow.michael.satstat/) or [GPSTest](https://f-droid.org/fr/packages/com.android.gpstest.osmdroid/)
- Sensors: [Sensor Test](https://play.google.com/store/apps/details?id=ru.andr7e.sensortest)
- Simcard info: [SIM Card Info](https://play.google.com/store/apps/details?id=me.harrygonzalez.simcardinfo)
- NFC: [NFC reader](https://f-droid.org/fr/packages/se.anyro.nfc_reader/)
- RootBeer: [RootBeer Fresh](https://play.google.com/store/apps/details?id=com.kimchangyoun.rootbeerFresh.sample)
- Network location: [RTR-NetTest 3G/4G/5G IPv4/6](https://play.google.com/store/apps/details?id=at.alladin.rmbt.android&hl=en&gl=US)
- DRM (widewine): https://play.google.com/store/apps/details?id=com.androidfung.drminfo
- Network Monitor: [PCAPDroid](https://f-droid.org/packages/com.emanuelef.remote_capture/)
- Media?
- https://developers.google.com/android/samples/code-samples (https://github.com/googlemaps/android-samples/tree/main/ApiDemos/kotlin)
- network performances: nperf
- Pcapdroid: https://f-droid.org/packages/com.emanuelef.remote_capture/
- Treble info: https://f-droid.org/packages/tk.hack5.treblecheck/

### Other

- Performances: [Geekbench](https://www.geekbench.com/)
- Dump OTA packages [dumpyara](https://github.com/AndroidDumps/dumpyara)
- Unpack and RePack AML/RK/AW ROMs [AmlogicKitchen](https://github.com/althafvly/AmlogicKitchen)
- Scan APK [virustotal](https://www.virustotal.com/gui/)
- edit map style: [maputnik](https://maputnik.github.io/)
- ih8sn [configs](https://github.com/althafvly/ih8sn/tree/master/system/etc)

### Demo

- [scrcpy](tools/scrcpy)

### Helpers

We developed some helpers at https://gitlab.e.foundation/e/os/android_dev_tools (Linux only)
- [screenrecord.sh](https://gitlab.e.foundation/e/os/android_dev_tools/-/blob/master/screenrecord.sh)
- [screenshot.sh](https://gitlab.e.foundation/e/os/android_dev_tools/-/blob/master/screenshot.sh)