+++
title = 'Pair Programming Exploration'
date = 2024-07-04T14:44:53+05:30
+++

# Exploration about how we can perform some pair programming at Murena

## Pair programming explanation:
very good and complete article:
https://dev.to/evergrowingdev/the-ultimate-guide-to-effective-pair-programming-5aej

another one about remote pair programming (RPP)
https://devopedia.org/remote-pair-programming

## Tools for Android studio pair programming

There is different kind of tools that could be used for RPP.
- Screensharing
- Screen control
- Cloud based IDE
- app sharing


### interesting tools
#### CodeTogether live
https://plugins.jetbrains.com/plugin/14225-codetogether-live

@vincent & @SayantanRC tried this plugin in Android studio. It "works well" but with a very limited scope. At least without more exploration. 

@hasibprince  & @fahim.choudhury  tried during 3 hours, in 3 sessions.

pros:
  - Easy to install
  - Easy to start
  - Voice sharing option, works well
  - Can share the code editor with a shared cursor (the driver action is reflected on backsitter screen: scrolling & typing) or each dev can have a dedicated cursor and both can edit a same time.
  - Client can see "run" panel output from host
  - 60 minutes for 1-3 client is free
  - No freeze or slow down. No particular latency
  - Screensharing possible from video calling tool with decent capabilities

cons:
  - Not sharing contextual menu (like right click panel)
  - Not sharing logcat panel
  - Not sharing code coloring neither android studio suggestion or error
  - Not sharing left panel (project file exploration)
  - The client cannot create new file or new package 
  - Not sharing autocompletion
  - Cannot share multiple windows. So works for a single opened project
  - Cannot share emulator window


note: 

> Editor part is much better for the Host

> What we found effective is that, opening the IDE code window and the call with screen sharing at the same time in two displays. Seeing the code being written in the call was better than in the IDE window.

Conclusion:
May be interesting for knowledge transfer, technical discussion toward a single project.
Too limited for something else

### NO GO tools

- [Duckly plugin](https://duckly.com/tools/android-studio)
  This plugin has very bad reviews in android studio plugin list.
  Some mentionning it as a spyware. Better to not try.
- **Whereby**
  Screen sharing through whereby doesn't allow the "client" to act, select line, etc.
  The tools is really not intended for that purpose.
- https://tuple.app/ (MacOs & windows tool)
- https://www.coscreen.co/blog/android-studio-collaboration/ (MacOs & Windows tool)
- [Code with me](https://www.jetbrains.com/code-with-me/buy/?section=personal&billing=yearly) Looks very good but not available with Android studio anymore [read more](https://plugins.jetbrains.com/plugin/14896-code-with-me/versions)

### other solution to explore

- https://jasonatwood.io/archives/2011
- Zoom 
- https://www.drovio.com/pricing/ (to pay)
- https://codeanywhere.com/ (cloud based & pay)
- https://git.live/


### Other tools for pair programming
<!-- global pair programming resources -->
- https://www.sitepoint.com/collaborative-coding-tools-for-remote-pair-programming/
- https://www.shakebugs.com/blog/collaborative-coding-tools/

