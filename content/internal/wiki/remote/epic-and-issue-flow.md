+++
title = 'Epic and Issue Flow'
date = 2024-07-04T14:44:39+05:30
+++

## Create an epic
Epic's list is available here: https://gitlab.e.foundation/groups/e/-/epics?state=opened&page=1&sort=start_date_desc

To create one, you need to ask to someone with the relevant permission.

## Asign an issue to an epic:

In an issue, in the right panel there is a "Epic" section. Just open the drop down list and select the one you want.
Example:

## From development purpose
*note:* Not sure it is relevant for all squads.

Create a branch for the epic on your project: <epic-number>-epic-<name you want>, from the main branch.
For each issue of the given epic, you will create your branch from this one and merge them into it.
Once every development is handled, use the latest build from this branch for test purpose.

For project to be embedded in android_prebuilt_prebuiltapk_lfs:
Create a dedicated branch for the epic and put the apk there.

You can now use this branch in /e/Os test build, thanks to the `FEATURE_BRANCH` flag.

Then you have to create a Test session issue, and to provide the build required for the test.
Here describe what has changed and what should be explicitely test.

For more details, contact QA squad for more details.


is the below content deprecated ? @rhunault 

```mermaid
graph TD
    S1[Awaiting specification] -->|design, functional, tech| S2[In specification]
    S2 --> S4[In planification]
    S4 --> S5[Awaiting development]
    S5 --> S6[In development]
    S6 --> S7[In validation]
    S7 --> |needs work| S5
    S7 --> |adjust specs/design| S1
    S7 --> |task done| S9
    S7 --> S8[Awaiting deployment]
    S8 --> |release| S9[closed]
```


## Epics vs issues

- Epics will include separate issues for translations and end-to-end QA
- Issues will contain translations in the development phase, before going for validation.



