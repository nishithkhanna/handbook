+++
title = 'Meetings'
date = 2024-07-04T14:46:37+05:30
+++

1. Each meeting must have an agenda
2. Each meeting should have expected inputs 
3. Each meeting should have expected outputs
4. Each meeting should be time bounded
5. Any attendee should join muted, **especially if he is late**
6. We encourage the usage of camera
7. You should raise your hand before speaking