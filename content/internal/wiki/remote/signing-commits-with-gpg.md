+++
title = 'Signing Commits With Gpg'
date = 2024-07-04T14:44:14+05:30
+++

GitLab has a great documentation for [signing commits with GPG](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits) that describes the process in details. This will be more of a quick reference / cheat sheet.

## TL;DR

assuming GPG is [installed](https://www.gnupg.org/download).

### for new commits:

- `gpg --gen-key` # generate key or `gpg --full-gen-key` for more options
- `gpg --list-secret-keys --keyid-format LONG $EMAIL` # get *KEYID*, the 16 alphanumeric string on `sec` line
- `gpg --armor --export $KEYID` # export aka print public key
- copy the public key. go to [GPG Keys · User Settings · e Foundation GitLab](https://gitlab.e.foundation/-/profile/gpg_keys). paste the key and click **Add key**
- `git config --global user.signingkey $KEYID` # tell git which key to use
- `git commit -S -m "My commit message"` # test GPG sign commit
- `git config --global commit.gpgsign true` # tell git to always sign commit with gpg
- `git log --show-signature` # verify it works

### for existing commits:
- `git checkout -b gpg-sign-$YOUR_NAME` # make a branch
- `git log` # to check the log and select a commit hash
- `git rebase -i $COMMIT_HASH` # to start interactive rebase or `git rebase -i --root` to start from beginning.
- `:%s/^pick/reword/gc` # replace pick with reword with vim
- `git commit --amend --no-edit` 
- `git rebase --continue`
- repeat last 2 steps until rebase is done.


## New commits
 
- [Creating a GPG key](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/#create-a-gpg-key)
	- ensure that GPG is [installed](https://www.gnupg.org/download) for the operating system.
	- generate your key pair with `gpg --gen-key` or `gpg --full-gen-key`
	- list key with `gpg --list-secret-keys --keyid-format LONG $EMAIL`
	- take note of the `sec` line. the 16 alphanumeric string is the *KEYID*
	- export the public key with `gpg --armor --export $KEYID`
- [Adding a GPG key to GitLab account](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/#add-a-gpg-key-to-your-account)
	- `gpg --armor --export $KEYID`  should show a long block of text starting with `-----BEGIN PGP PUBLIC KEY BLOCK-----` and ending with `-----END PGP PUBLIC KEY BLOCK-----`. That is what we need for now.
	- to add key to GitLab; Sign in to [e Foundation GitLab](https://gitlab.e.foundation) -> In the top-right corner, select your avatar -> Select **Edit profile** -> On the left sidebar, select [GPG Keys](https://gitlab.e.foundation/-/profile/gpg_keys) -> In Key, paste the ***public*** key ->  select **Add key**. Again, add the ***public*** key, **NOT** the ***private*** key. On success, it should be like this:
	![successfully adding gpg public key](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/img/profile_settings_gpg_keys_single_key.png)
- [Associating the GPG key with Git](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/#associate-your-gpg-key-with-git)
	- assuming the *KEYID* is known from the previous step, assign the key with the user `git config --global user.signingkey $KEY_ID`
	- optionally assign GPG program with `git config --global gpg.program gpg2`
- [Signing Git commits](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/#sign-your-git-commits)
	- now commits can be with `git commit -S -m "My commit message"`
	- the `-S` can be skipped by making `gpgsign` flag true with `git config --global commit.gpgsign true`
- [Verifying that signing works](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/#verify-commits)
	- Go to Commits from Project or MR and it should show either a Verified or Unverified badge, depending on the verification status of the GPG signature like this:
	![verified or unverified](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/img/project_signed_and_unsigned_commits.png)
	- additionally, signed commits can be check from `git` CLI with `git log --show-signature`

## Existing commits

What if we need to sign our old, existing commits with GPG? That is possible too. To do that:

- Go to the project you want to sign from `git` CIL
- make a branch `git checkout -b gpg-sign-$YOUR_NAME`
- See log with `git log`
- select a commit where you want to start with
- start an interactive rebase with `git rebase -i $COMMIT_HASH` or `git rebase -i --root` if you want start from begining.
- we want to ‘reword’ the commits. so we do `r` for all the commits we want to sign. assuming git commit opens `vim` we can do with `:%s/^pick/reword/gc` the save with `wq`
- now start signing with `git commit --amend --no-edit` then `git rebase --continue` and repeat until rebase is done.

## Troubleshooting
- `error: gpg failed to sign the data fatal: failed to write commit object`?
	-  ensure that basic encryption with `gpg` works: `echo "test" | gpg --clearsign`
- the GPG sign gets lost when the MR is merged, as there is no way to add a GPG key signature on the Web. related issues:
    - [Generate GPG signature for commit when edit a file in web editor](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/36944)
    - [Automated/web commits (merge or web IDE commits) should be OpenPGP signed](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/36619) > moved to > [Automated/web commits (merge or web IDE commits) should be OpenPGP signed](https://gitlab.com/gitlab-org/gitlab/-/issues/19185)
- This seems very Linux specific and needs a lot of CIL command to be used. I would like to use GUI tool to manage the key or achieve the goal.
   - Once `git` and `gnupg` is installed, the procedure is the same for all environments. Even after that, you fear the CLI, try out a few GUI tools and let us know what worked best by adding it in this wiki. Found this one for Windows: https://github.com/nextcloud/server/wiki/How-to-sign-your-commits-using-PGP
- I use X IDE. How can I use GPG with it?
  - It would be out of scope for this wiki to add the process for all IDEs. If you find any good guide for popular IDE's like VS Code, JetBrains, please feel free to edit the wiki and extend it.



### source:
- [Signing commits with GPG | GitLab](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/)
- [Signing Existing Commits with GPG - DEV Community | Joseph D. Marhee](https://dev.to/jmarhee/signing-existing-commits-with-gpg-5b58)
- [Troubleshooting GPG git commit signing - Julian Simioni](https://juliansimioni.com/blog/troubleshooting-gpg-git-commit-signing/)

