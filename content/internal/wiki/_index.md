---
title: 'Wiki'
cascade:
  type: docs
---


Welcome to the /e/'s internal wiki. This wiki documents some important details regarding the overall project development which is supposed to be used by the **/e/** internal development team to develop and maintain the project. Pages are grouped by sections which are arranged in alphabetical order.

> **How to contribute ?**
>
> This wiki is open to contribution from any team member having access to it. Feel free to modify any information.
>
> ⚠️ Please **do not edit the page title**, as it will modify the page url!


## 👥 People

- 🗺 [team map](https://gitlab.e.foundation/teams/team-map/-/boards/666)
- [Onboarding](people/onboarding)
- [Time off](people/time-off)
- [Glossary](people/glossary)
- [Contractor invoice process](contractor-invoice-process)
- [Employee and contractor purchase program](Employee-and-Contractor-purchase-program)
<!-- - outboarding -->
<!-- - values -->
<!-- - code of conduct/values -->


## [🧰 Tools](tools/tools)

## 👨‍💻 Hiring

- [Recruitment process](recruitment/recruitment-process)
<!-- - job description -->

## 📘 Management

<!-- - Annual review -->
<!-- - Roles -->
- [1:1](management/1-to-1)

## 🏠 Remote

<!-- - timezone -->
<!-- - documentation -->
- [Signing commits with GPG](remote/signing-commits-with-gpg)
<!-- - asynchronous communication -->
<!-- - automation -->
<!-- - calls -->
- [Meetings](meetings)
- [Epic and issue flow](remote/Epic-and-issue-flow)
<!-- remote dev-->
- [Pair programming](remote/pair-programming-exploration)

## 🫂 Teams

🗺 [team map](https://gitlab.e.foundation/teams/team-map/-/boards/666)

- [📢 Communication](communication-team/Communication-team)
- [👤 Community](community-team/Community-team)
- [🎨 Design](design-team/Design-team)
- [👨‍💻 Development](Development-team)
- [📝 Product](Product-team)
- [🔬 QA](qa-team/QA-team)
- [🏋️ Support](support-team/Support-team)

## ⚔️ Guilds

- [Developer blog](guilds/Developer-blog-guild)

---

**Ressources**
- [Remote Handbook](https://www.notion.so/Remote-Handbook-a3439c6ccaac4d5f8c7515c357345c11)
- [Slite Handbook](https://sliteuniverse.slite.page/p/C01J9MHHz/Team-HQ)
- [The Ultimate Guide To Remote work (Zapier)](https://cdn.zapier.com/storage/learn_ebooks/e4fbeb81f76c0c13b589cd390cb6420b.pdf)
- [GitLab Handbook](https://about.gitlab.com/handbook/)
