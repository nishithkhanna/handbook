+++
title = 'Recruitment Process'
date = 2024-07-04T14:42:43+05:30
+++

**Never forget, that there is real people behind the mail**

[[_TOC_]]

## [Ongoing recruitments](https://murena.com/jobs/)

- AOSP Framework 
- Backend web developer
- Infrastructure Engineer / System Administrator

## Guidelines

1. **If there is a doubt, there is no doubt**: if you have a doubt about a candidate (aka you are not conviced), let's stop the recruitment process with him
2. **The candidate may become your colleague**: always keep in mind you will have to work with the candidate. It means in addition to the technical skills, the candidate has to fit with the team and even more the squad.
3. **Give a good image of the Murena team**: the recruitment process will be the first insight a candidate will have of the project. Do your best for this first contact to be as good as possible. Some keywords: polite, reactivity, clarity

## Process

### 1. Job offers

For open positions, the team publish jobs offers at https://murena.com/jobs/.

1. The manager creates a document at [recruitment murena.io folder](https://murena.io/f/53184423) to draft the job offer
2. Once approved by management, the management has to create an issue at [ecorp/web/website](https://gitlab.e.foundation/ecorp/web/website/-/issues/new) to publish the job offer
3. Gaël can communicate on social media, and eventually use LinkedIn

### 2. Job applications triage

We check job applications only coming from techjobs@e.email

1. On a daily basis, the manager checks the new job applications at https://helpdesk.murena.com/#ticket/view/tech_jobs
2. If the applications doesn't seems to match the job offer, let's inform the candidate we decline the application with `::decline-job-application`  text module
3. For remaining applications
    1. we tag as follows in order to forward the application to the corresponding squad
        - `role:AOSP`
        - `role:android`
        - `role:embedded`
        - `role:sysadmin`
        - `role:web-dev`
        - `role:network`
        - `role:reverse-engineering`
        - `role:chromium`
    2. *Ideally* we acknowledge the application to the candidate using `::application-received` text module


### 3. Squad triage process

Each squad applies its own recruitment process, which most of the time has the following steps:
1. First triage on the CV + resume. Feel free to ask question at any time to the candidate
1. A technical exercise
1. A technical interview


### 4. Management triage process

The management runs a last interview with the candidate(s) selected by the squad. The goal is to:
1. Give context about the project
2. Validate the profile fit within the team
3. Align on a rate and a start date

### 5. Contract

There are multiple options here, depending of the position and the location of the candidate.

### 6. [Onboarding](https://gitlab.e.foundation/internal/wiki/-/wikis/onboarding/Onboarding)

1. Create an onboarding issue at https://gitlab.e.foundation/ecorp/management/hr/-/issues/new
2. Before the onboarding, send the following email:

```
Hi ______,


Kindly find bellow some information for the onboarding on the /e/ project.

What you have to do is:

    install the app Telegram (https://telegram.org/) and ping @rhunault and me (@magpieVince)
    create a Gitlab account at https://gitlab.e.foundation/users/sign_in?redirect_to_referer=yes, and forward me your user ID
    Read the documentation (see below) when you will have access to it.
    note any questions you may have. You will be able to ask them during the welcome call. We will do it ______.

See you _____ !


Tools
https://gitlab.e.foundation/internal/wiki/-/wikis/Onboarding/Tools

Don't forget to create every account indicated.
Communication
https://gitlab.e.foundation/internal/wiki/-/wikis/Onboarding/Communication
Team

We miss a real team map, but you can find contact list at
https://gitlab.e.foundation/teams/team-map/-/boards/666

Useful links
https://gitlab.e.foundation/internal/wiki/-/wikis/Onboarding/Useful-links

Yours sincerly

```